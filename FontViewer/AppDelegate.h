//
//  AppDelegate.h
//  FontViewer
//
//  Created by Roman Fedyanin on 27/05/14.
//  Copyright (c) 2014 Mendax. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
